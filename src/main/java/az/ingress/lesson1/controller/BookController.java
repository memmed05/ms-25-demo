package az.ingress.lesson1.controller;

import az.ingress.lesson1.dto.BookGetAllResponse;
import az.ingress.lesson1.dto.BookRequestDto;
import az.ingress.lesson1.dto.BookResponseDto;
import az.ingress.lesson1.service.BookService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/api/v1/book")
public class BookController {

    private final BookService bookService;

    public BookController(@Qualifier("bookServiceImpl") BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookResponseDto> get(@PathVariable Integer id,
                                               HttpServletRequest request) {
        BookResponseDto res = bookService.get(id);
        return ResponseEntity.ok(res);
    }

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody BookRequestDto dto) {
        int id = bookService.create(dto);
        return ResponseEntity.created(URI.create("book/" + id)).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<BookResponseDto> update(@PathVariable Integer id,
                                                  @RequestBody BookRequestDto dto) {
        BookResponseDto response = bookService.update(id, dto);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable Integer id) {
        bookService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<BookGetAllResponse> getAll() {
        return ResponseEntity.ok(bookService.getAll());
    }
}
